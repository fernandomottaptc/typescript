"use strict";
class Data {
    constructor(dia = 1, mes = 1, ano = 1970) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }
}
const aniversario = new Data(22, 12, 1989);
console.log(aniversario.dia);
console.log(aniversario);
const casamento = new Data;
casamento.ano = 2017;
console.log(casamento);
// class Data {
//
//     dia: number
//     mes: number
//     ano: number
//
//     constructor(dia: number = 1, mes: number = 1, ano: number = 1970) {
//         this.dia = dia
//         this.mes = mes
//         this.ano = ano
//     }
// }
//
// const aniversario = new Data(22,12,1989)
// console.log(aniversario.dia)
// console.log(aniversario)
//
// const casamento = new Data
// casamento.ano = 2017
// console.log(casamento)
console.log('--------------------------');
class Produto {
    constructor(nome, preco, desconto = 0) {
        this.nome = nome;
        this.preco = preco;
        this.desconto = desconto;
    }
    precoComDesconto() {
        return this.preco * (1 - this.desconto);
    }
    resumo() {
        return `${this.nome} custa R$${this.precoComDesconto()} (${this.desconto * 100})`;
    }
}
const prod1 = new Produto('Caneta Bic Preta', 4.20);
prod1.desconto = 0.06;
console.log(prod1);
console.log(prod1.resumo());
const prod2 = new Produto('Caderno Escolar', 20.00, 0.15);
console.log(prod2);
console.log(prod2.resumo());
console.log('--------------------------');
class Carro {
    constructor(marca, modelo, velocidadeMaxima = 200) {
        this.marca = marca;
        this.modelo = modelo;
        this.velocidadeMaxima = velocidadeMaxima;
        this.velocidadeAtual = 0;
    }
    alterarVelocidade(delta) {
        const novaVelocidade = this.velocidadeAtual + delta;
        const velocidadeValida = novaVelocidade >= 0 && novaVelocidade <= this.velocidadeMaxima;
        if (velocidadeValida) {
            this.velocidadeAtual = novaVelocidade;
        }
        else {
            this.velocidadeAtual = delta > 0 ? this.velocidadeMaxima : 0;
        }
        return this.velocidadeAtual;
    }
    acelerar() {
        return this.alterarVelocidade(5);
    }
    frear() {
        return this.alterarVelocidade(-5);
    }
}
const carro1 = new Carro('Ford', 'Ka', 185);
Array(50).fill(0).forEach(() => carro1.acelerar());
console.log(carro1.acelerar());
Array(20).fill(0).forEach(() => carro1.frear());
console.log(carro1.frear());
console.log('--------------------------');
class Ferrari extends Carro {
    constructor(modelo, velocidadeMaxima) {
        super('Ferrari', modelo, velocidadeMaxima);
    }
    acelerar() {
        return this.alterarVelocidade(20);
    }
    frear() {
        return this.alterarVelocidade(-15);
    }
}
const f40 = new Ferrari('F40', 330);
console.log(`${f40.marca} / ${f40.modelo} / ${f40.velocidadeMaxima}`);
console.log(f40.acelerar());
console.log(f40.frear());
console.log('--------------------------');
// BOM PARA FAZER VALIDAÇÕES.......
class Pessoa {
    constructor() {
        this._idade = 0;
    }
    get idade() {
        return this._idade;
    }
    set idade(vlr) {
        if (vlr >= 0 && vlr <= 120) {
            this._idade = vlr;
        }
    }
}
const pessoa1 = new Pessoa;
pessoa1.idade = 10;
console.log(pessoa1);
console.log(pessoa1.idade);
console.log('--------------------------');
// atributos e metodos estáticos
// class Matematica {
//     PI: number = 3.1416
//
//     areaCirc(raio: number): number {
//         return this.PI * raio * raio
//     }
// }
//
// const m1 = new Matematica()
// m1.PI = 4.2
// console.log(m1.areaCirc(4))
class Matematica {
    static areaCirc(raio) {
        return this.PI * raio * raio;
    }
}
Matematica.PI = 3.1416;
console.log(Matematica.areaCirc(4));
